import { Server, Request, ResponseToolkit } from "@hapi/hapi";
import ClientOAuth2 from 'client-oauth2'

const stravaAuthClient = new ClientOAuth2({
    clientId: "",
    clientSecret: "",
    redirectUri: "http://localhost:3000/callback",
    accessTokenUri: "https://www.strava.com/oauth/token",
    authorizationUri: "https://www.strava.com/oauth/authorize",
    scopes: ["activity:read_all"]
});

const routes = [
    {
        method: 'GET',
        path: '/proxy/{path*}',
        handler: async (request: Request, h: ResponseToolkit) => {
            return h.response("http://localhost:8080" + request.path + request.url.search)
        }
    },
    {
        method: 'GET',
        path: '/callback',
        handler: async (request: Request, h: ResponseToolkit) => {
            try {
                const response = await stravaAuthClient.code.getToken(request.url)
                return h.response({
                    accessToken: response.accessToken
                })
            } catch(error) {
                console.log(error)
                return h.response("An error occurred").code(500)
            }
        }
    },
    {
        method: 'GET',
        path: '/sign-in',
        handler: (_: Request, h: ResponseToolkit) => {
            const response = stravaAuthClient.code.getUri()
            return h.redirect(response)
        }
    }
]

const init = async () => {
    const server: Server = new Server({
        port: 3000,
        host: 'localhost'
    });

    server.route(routes);

    await server.start();
    
    console.log('Server running on %s', server.info.uri);
};
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();